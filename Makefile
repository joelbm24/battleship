tests:
	mkdir -p bin
	g++ -o bin/tests test/test.cpp

clean:
	rm bin/tests
