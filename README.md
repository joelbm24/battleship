A Battleship game written in C++ using Domain Driven Design and Test Driven Development.

A good talk on the approach that i use.

[Talk by Uncle Bob](https://www.youtube.com/watch?v=HhNIttd87xs)

##Compilation

###Tests
To compile the tests type:

```
#!bash
$ make tests
```

##Running

###Tests
To run the tests type:

```
#!bash
$ bin/tests
```

from the root of the directory.

##TODOs
- after making a game entity, add a save repo so a user can leave and pick up a game.
- board should be split into a game entity and a board entity in theory a game can have more than one board.
- Add message queue.
- finish implementing the addRandomShips message in the board entity.
- seperate out implementation tests from domain tests.
