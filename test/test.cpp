#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "../lib/battleship.hpp"

TEST_CASE("Board", "[board]") {
  
  SECTION("can get width") {
    Board board = Board(10,10);
    REQUIRE(board.getWidth() == 10);
  }

  SECTION("can get height") {
    Board board = Board(10,10);
    REQUIRE(board.getHeight() == 10);
  }

  SECTION("can add ship") {
    Board board = Board(10,10);
    Ship ship = Ship(Point(1,1), "vertical", 3);
    board.addShip(ship);

    REQUIRE(board.getAmountOfShips() == 1);
  }

  SECTION("can sink ship") {
    Board board = Board(10,10);
    Ship ship = Ship(Point(1,1), "vertical", 2);

    board.addShip(ship);

    board.fireAt(Point(1,1));
    REQUIRE(board.getAmountOfHits() == 1);

    board.fireAt(Point(1,2));
    REQUIRE(board.getAmountOfHits() == 2);
    REQUIRE(board.getAmountOfSunkenShips() == 1);
  }

  SECTION("can get amount of misses") {
    Board board = Board(10,10);
    Ship ship = Ship(Point(1,1), "vertical", 2);

    board.addShip(ship);
    board.fireAt(Point(6,6));

    REQUIRE(board.getAmountOfMisses() == 1);
    REQUIRE(board.getAmountOfHits() == 0);
  }

  SECTION("can get amount of hits") {
    Board board = Board(10,10);
    Ship ship = Ship(Point(1,1), "vertical", 2);
    
    board.addShip(ship);
    board.fireAt(Point(1,2));
    REQUIRE(board.getAmountOfHits() == 1);
  }
}

TEST_CASE("Game", "[game]") {

  SECTION("setup") {
    Game game = Game(10,10);
  }

  SECTION("catch when out of bounds") {
    Game game = Game(10,10);
    game.fireAt(Point(20,20));
    REQUIRE(game.getMessage() == "You can't fire there");
    game.fireAt(Point(1,1));
    REQUIRE(game.getMessage() == "You fired!");
  }

  SECTION("catch when point is already fired at") {
    Game game = Game(10,10);

    game.fireAt(Point(2,2));
    REQUIRE(game.getMessage() == "You fired!");

    REQUIRE(game.getMessageCount() == 0);

    game.fireAt(Point(2,2));
    REQUIRE(game.getMessage() == "You can't fire there");

  }

}

TEST_CASE("MessageQueue", "[queue]") {
  SECTION("Can add message") {
    MessageQueue message_queue = MessageQueue();
    message_queue.addMessage("Here's a message");
    std::string message = "Here's a message";

    REQUIRE(message_queue.getMessageCount() == 1);
    REQUIRE(message_queue.getMessage() == message);
    REQUIRE(message_queue.getMessageCount() == 0);
  }

  SECTION("can delete after getting a message more than once") {
    MessageQueue message_queue = MessageQueue();
    message_queue.addMessage("a");
    REQUIRE(message_queue.getMessage() == "a");

    message_queue.addMessage("b");
    REQUIRE(message_queue.getMessage() == "b");
  }
}

TEST_CASE("Ship", "[ship]") {

  SECTION("can access a point for a vertical ship") {
    Ship ship = Ship(Point(1,2), "vertical", 2);
    REQUIRE(ship.hasPoint(Point(1,3)) == true);
  }

  SECTION("can acess a point for a horizontal ship") {
    Ship ship = Ship(Point(3,4), "horizontal", 3);

    REQUIRE(ship.hasPoint(Point(4,4)) == true);
  }

  SECTION("can sink ship") {
    Ship ship = Ship(Point(1,2), "vertical", 2);

    ship.hit();
    ship.hit();

    REQUIRE(ship.isSunk() == true);
  }

  SECTION("check if two ships are equal") {
    Ship ship1 = Ship(Point(1,2), "vertical", 2);
    Ship ship2 = Ship(Point(2,2), "vertical", 3);
    Ship ship3 = Ship(Point(1,2), "vertical", 2);
    Ship ship4 = Ship(Point(2,2), "horizontal", 3);
      


    REQUIRE((ship1 == ship2) == false);
    REQUIRE((ship1 == ship3) == true);
    REQUIRE((ship2 == ship4) == false);
  }

}

TEST_CASE("Point", "[point]") {
  Point point = Point(1,2);

  SECTION("getting x value") {
    REQUIRE(point.getX() == 1);
  }

  SECTION("getting y value") {
    REQUIRE(point.getY() == 2);
  }

  SECTION("seeing if equal") {
    Point point2 = Point(1,2);

    REQUIRE(point == point2);
  }

  SECTION("seeing if unequal") {
    Point point2 = Point(1,3);
    Point point3 = Point(4,5);

    REQUIRE(point != point2);
    REQUIRE(point != point3);
  }

}

