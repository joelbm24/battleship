#include "../entities/board.hpp"

class ConsoleInteracter {
  public:
    ConsoleInteracter(int columns, rows);

    void
      fireAt(int column, int row);

    Map<Point, string>
      boardInfo

  private:
    int const
      MAX_AMOUNT_OF_COLUMNS,
      MAX_AMOUNT_OF_ROWS;

    Board
      board;

};

ConsoleInteracter::ConsoleInteracter(int columns, int rows ) {
  board = Board(columns, rows);
  board.addRandomShips();

  MAX_AMOUNT_OF_COLUMNS = columns;
  MAX_AMOUNT_OF_ROWS = rows;
}

void
ConsoleInteracter::fireAt(int column, int row) {
  board.fireAt(Point(column, row));
}


ConsoleInteracter::getBoardInfo() {

}
