class Point {
  public:
    Point(int, int);

    int
      getX(),
      getY();

    bool
      operator==(const Point &other_point),
      operator!=(const Point &other_point);

  private:
    int
      x,
      y;

};

Point::Point(int initial_x, int initial_y) {
  x = initial_x;
  y = initial_y;
}

int
Point::getX() {
  return x;  
}

int
Point::getY() {
  return y;
}

bool
Point::operator==(const Point &other_point) {
  return (x == other_point.x) && (y == other_point.y);
}

bool
Point::operator!=(const Point &other_point) {
  return !((x == other_point.x) && (y == other_point.y));
}
