#include "board.hpp"
#include "message_queue.hpp"

class Game {
  public:
    Game(int, int);

    void
      fireAt(Point point),
      addRandomShips();

    std::string
      getMessage();

    int
      getMessageCount();

  private:
    int
      width,
      height;

    std::vector<Board>
      boards;

    MessageQueue
      message_queue;

    bool
      isPointMissed(Point point),
      isPointHit(Point point),
      isNotAlreadyFiredAt(Point point),
      canFireAtPoint(Point point),
      isWithinBounds(Point point),
      vectorHasPoint(std::vector<Point> points, Point point);
};

Game::Game(int w, int h) {
  width = w;
  height = h;
  message_queue = MessageQueue();
  boards.push_back(Board(w,h));
}

void
Game::fireAt(Point point) {
  if(canFireAtPoint(point)) {
    boards.at(0).fireAt(point);
    message_queue.addMessage("You fired!");
  }
  else {
    message_queue.addMessage("You can't fire there");
  }
}

bool
Game::canFireAtPoint(Point point) {
  return (isWithinBounds(point) && isNotAlreadyFiredAt(point));
}

bool
Game::isWithinBounds(Point point) {
  return (point.getX() > 0 && point.getX() < width) &&
    (point.getY() > 0 && point.getY() < height);
}

bool
Game::isNotAlreadyFiredAt(Point point) {
  return !(isPointMissed(point) || isPointHit(point));
}

bool
Game::isPointMissed(Point point) {
  return vectorHasPoint(boards.at(0).getMisses(), point);
}

bool
Game::isPointHit(Point point) {
  return vectorHasPoint(boards.at(0).getHits(), point);
}

bool
Game::vectorHasPoint(std::vector<Point> points, Point point) {
  bool
    value = false;

  for(int i=0; i < points.size(); i++) {
    if (points.at(i) == point)
      value = true;
  }

  return value;
}

std::string
Game::getMessage() {
  return message_queue.getMessage();
}

int
Game::getMessageCount() {
  return message_queue.getMessageCount();
}
