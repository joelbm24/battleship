#include "../values/point.hpp"
#include <string>
#include <vector>

class Ship {
  public:
    Ship(Point p, std::string o, int l);

    bool
      hasPoint(Point point),
      isSunk();

    void
      hit();

    bool
      operator==(const Ship &other_ship);
      //operator!=(const Point &other_point);

  private:
    std::vector<Point>
      points;

    std::string
      orientation;

    bool
      sunken;

    int
      length,
      number_of_hits;

    void
      setPoints(Point p);
};

Ship::Ship(Point p, std::string o, int l) {
  orientation = o;
  length = l;
  number_of_hits = 0;
  sunken = false;
  setPoints(p);
}

void
Ship::setPoints(Point p) {
  //TODO: Refactor it does two similar things.

  points.push_back(p);

  if (orientation == "vertical") {
    for (int i=1; i<=length; i++) {
      points.push_back(Point(p.getX(), p.getY()+i));
    }
  }
  if (orientation == "horizontal") {
    for (int i=1; i<=length; i++) {
      points.push_back(Point(p.getX()+i, p.getY()));
    }
  }
}

bool
Ship::hasPoint(Point point) {
  bool
    value = false;

  for(int i = 0; i<length; i++) {
    if (point == points.at(i))
      value = true;
  }

  return value;
}

bool
Ship::isSunk() {
  return sunken;
}

void
Ship::hit() {
  number_of_hits += 1;

  if(number_of_hits == length)
    sunken = true;
}

bool
Ship::operator==(const Ship &other_ship) {
  return (length==other_ship.length) &&
    (orientation == other_ship.orientation) &&
    (points.at(0) == other_ship.points.at(0));
}

/*bool
Ship::operator!=(const Ship &other_ship) {
  return !((x == other_point.x) && (y == other_point.y));
}*/
