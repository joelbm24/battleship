#include "ship.hpp"

class Board {
  public:
    Board(int w, int h);

    int
      getWidth(),
      getHeight(),
      getAmountOfHits(),
      getAmountOfMisses(),
      getAmountOfShips(),
      getAmountOfSunkenShips();

    std::vector<Point>
      getHits(),
      getMisses();

    void
      addShip(Ship ship),
      fireAt(Point point);

  private:
    std::vector<Ship>
      ships;

    std::vector<Point>
      hits,
      misses;

    int
      width,
      height;

    void
      hitShip(Ship * ship, Point point),
      missShip(Point point);

};

Board::Board(int w, int h) {
  width = w;
  height = h;
}

int
Board::getWidth() {
  return width;
}

int
Board::getHeight() {
  return height;
}

int
Board::getAmountOfHits() {
  return hits.size();
}

int
Board::getAmountOfMisses() {
  return misses.size();
}

int
Board::getAmountOfShips() {
  return ships.size();
}

int
Board::getAmountOfSunkenShips() {
  int value = 0;

  for (int i=0; i<getAmountOfShips(); i++) {
    if(ships[i].isSunk())
      value += 1;
  }

  return value;
}

std::vector<Point>
Board::getHits() {
  return hits;
}

std::vector<Point>
Board::getMisses() {
  return misses;
}

void
Board::addShip(Ship ship) {
  ships.push_back(ship);
}

void
Board::fireAt(Point point) {
  for(int i=0; i<ships.size(); i++) {
    if(ships.at(i).hasPoint(point))
      hitShip(&(ships.at(i)), point);
    else
      missShip(point);
  }
}

void
Board::hitShip(Ship * ship, Point point) {
  ship->hit();
  hits.push_back(point);
}

void
Board::missShip(Point point) {
  misses.push_back(point);
}

