#include <vector>
#include <string>

class MessageQueue {
  public:
    MessageQueue();

    void
      addMessage(std::string message);

    std::string
      getMessage();

    int
      getMessageCount();

  private:
    std::vector<std::string>
      messages;
};

MessageQueue::MessageQueue() {
}

void
MessageQueue::addMessage(std::string message) {
  messages.push_back(message);
}

std::string
MessageQueue::getMessage() {
  std::string
    message = messages.front();

  messages.erase(messages.begin());

  return message; 
}

int
MessageQueue::getMessageCount() {
  return messages.size();
}
